#!/bin/bash


export image="$1"

# Get tags from upstream
upstream_json_resp="$(skopeo inspect docker://"${image}" | jq . -c)"
upstream_repo_image=$(echo "${upstream_json_resp}" | jq .Name -r)
image_name=$(basename "${upstream_repo_image}")
upstream_image_repo=$(dirname "${upstream_repo_image}")
tags_to_sync=$(echo "${upstream_json_resp}" | jq -r '.RepoTags[] ')

# Get tags from CERN
cern_json_resp="$(skopeo inspect docker://${CI_REGISTRY_IMAGE}/$(basename ${image}) | jq . -c)"
cern_repo_image=$(echo "${cern_json_resp}" | jq .Name -r)
cern_repo_tags="$(echo "${cern_json_resp}" | jq .RepoTags[] -r | xargs)"

# Start update process
echo "start to sync ${upstream_image_repo}/${image_name}"
for tag in $(echo "${tags_to_sync[@]}" ); do
  echo -n "syncing ${upstream_image_repo} % ${image_name} % ${tag}"
  i=0
  update=false
  # If the container tag does not exist at CERN
  if [[ ! " ${cern_repo_tags[@]} " =~ " ${tag} " ]]; then
    update=true
  else
    upstream_digest="$(skopeo inspect docker://"${image}" | jq .Digest)"
    cern_digest="$(skopeo inspect docker://${CI_REGISTRY_IMAGE}/$(basename ${image}) | jq .Digest)"
    if [ "$upstream_digest" != "$cern_digest" ]; then
      update=true
    else
      # Nothing to do
      echo " - exists"
    fi
  fi

  if [ "$update" = true ]; then
    # Update
    echo ""
    until skopeo copy --dest-creds "gitlab-ci-token:${CI_BUILD_TOKEN}" "docker://${upstream_image_repo}/${image_name}:${tag}" "docker://${CI_REGISTRY_IMAGE}/${image_name}:${tag}"
    do
      ((i++))
      if [ $i -gt 5 ] ; then
        break
      fi
      sleep 2
    done
  fi
done
echo "end syncing ${upstream_image_repo}/${image_name}"
